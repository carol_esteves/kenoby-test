# Desafio Frontend Kenoby

Este projeto tem como intuito desenvolver uma interação de busca de vagas utilizando a API da [Kenoby](http://www.kenoby.com/). O objetivo do usuário é encontrar as posições que tem a ver com a sua carreira.

## URL para Live Test

[http://frontendtestkenoby.surge.sh/](http://frontendtestkenoby.surge.sh/)

## Comandos Básicos

Para fazer download do projeto, siga as instruções:

```
> git clone git@bitbucket.org:carol_esteves/kenoby-test.git
> cd kenoby-test
> npm install
> npm start
```

### `npm start`

Roda o projeto em modo de desenvolvimento.
Abra [http://localhost:3000](http://localhost:3000) para ver o projeto no browser.


### `npm test`

Inicia os testes em `watch mode`.

### `npm run build`

Roda o build de produção, que vai para a pasta `build`.

## Features

Na descrição do teste foram solicitadas algumas features, como: adicionar uma vaga aos Favoritos, filtrar por Favoritos, e realizar uma busca por palavra-chave. Todas as features foram feitas localmente, utilizando o `state` dos componentes.

### Nota

Esta aplicação foi iniciada utilizando o [Create React App](https://github.com/facebook/create-react-app). Os componentes de layout foram feitos com [Material-UI](https://material-ui.com/).
