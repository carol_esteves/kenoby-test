import React, { Component } from 'react'
import Positions from './containers/positions'
import './App.css'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <Positions />
      </div>
    )
  }
}

export default App
