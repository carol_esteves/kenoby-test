import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Search from '../../../components/common/search'

Enzyme.configure({ adapter: new Adapter() })

describe('Search component', () => {
  it('Should render correctly', () => {
    const props = {
      classes: {},
      onSearch: () => console.debug('onSearch')
    }

    shallow(<Search {...props} />)
  })
})
