import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Switch from '../../../components/common/switch'

Enzyme.configure({ adapter: new Adapter() })

describe('Switch component', () => {
  it('Should render correctly', () => {
    const props = {
      showBookmarked: true,
      toggleBookmark: () => console.debug('toggleBookmark')
    }

    shallow(<Switch {...props} />)
  })
})
