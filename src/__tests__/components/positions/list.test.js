import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import PositionsList from '../../../components/positions/list'

const getFixture = name => require(`../../fixtures/${name}.json`)

const fixture = {
  positions: getFixture('positions/positions')
}

Enzyme.configure({ adapter: new Adapter() })

describe('Positions list component', () => {
  it('Should render correctly', () => {
    const props = {
      loading: true,
      bookmarked: true,
      positions: fixture.positions,
      classes: {},
      handleBookmark: () => console.debug('handleBookmark')
    }

    shallow(<PositionsList {...props} />)
  })
})
