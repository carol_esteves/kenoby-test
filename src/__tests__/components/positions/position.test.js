import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Position from '../../../components/positions/position'

const getFixture = name => require(`../../fixtures/${name}.json`)

const fixture = {
  positions: getFixture('positions/positions')
}

Enzyme.configure({ adapter: new Adapter() })

describe('Positions list component', () => {
  it('Should render correctly', () => {
    const { id, name, description, links } = fixture.positions[0]
    const props = {
      id,
      name,
      description,
      links,
      isBookmarked: true,
      classes: {},
      handleBookmark: () => console.debug('handleBookmark')
    }

    shallow(<Position {...props} />)
  })
})
