import React from 'react'
import { getPositionsRequest } from '../../requests/positions'
import { PositionsList } from '../../components/positions'
import Header from '../../components/common/header'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { contains } from '../../helpers/utils'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: { main: '#9013fe' },
    secodary: { main: '#9013fe' }
  }
})

const initialState = {
  positions: [],
  loading: true,
  bookmarked: [],
  showBookmarked: false,
  searchTerm: ''
}

class Positions extends React.Component {
  constructor () {
    super()

    this.state = initialState
  }

  normalizePositions = positions => (
    positions.reduce((acc, p) => {
      return [...acc, {
        id: p._id,
        name: p.name,
        description: p.description,
        links: p.linksExternal
      }]
    }, [])
  )

  componentDidMount () {
    getPositionsRequest().then(res => {
      const { data } = res

      this.setState(prevState => ({
        ...prevState,
        positions: [
          ...prevState.positions,
          ...this.normalizePositions(data)
        ],
        loading: false
      }))
    })
  }

  bookmarkPosition = id => {
    this.setState(prevState => ({
      ...prevState,
      bookmarked: prevState.bookmarked.includes(id)
        ? [ ...prevState.bookmarked.filter(i => i !== id) ]
        : [ ...prevState.bookmarked, id ]
    }))
  }

  filterBookmarked = (positions, bookmarked) => (
    positions.filter(({ id }) => (
      bookmarked.includes(id)
    ))
  )

  toggleBookmarked = () => {
    this.setState(prevState => ({
      ...prevState,
      showBookmarked: !prevState.showBookmarked
    }))
  }

  filterPositions = positions => {
    const { searchTerm } = this.state
    return searchTerm
      ? positions.filter(p => contains(p.name, searchTerm))
      : positions
  }

  search = term => {
    this.setState(prevState => ({
      ...prevState,
      searchTerm: term
    }))
  }

  render () {
    const {
      positions,
      loading,
      bookmarked,
      showBookmarked,
      searchTerm
    } = this.state

    const pos = showBookmarked
      ? this.filterBookmarked(positions, bookmarked)
      : positions

    const mergedProps = {
      loading,
      bookmarked,
      positions: searchTerm ? this.filterPositions(pos) : pos
    }

    return (
      <section role='main'>
        <MuiThemeProvider theme={theme}>
          <Header
            showBookmarked={showBookmarked}
            toggleBookmark={this.toggleBookmarked}
            search={this.search}
          />
          <PositionsList
            {...mergedProps}
            handleBookmark={this.bookmarkPosition}
          />
        </MuiThemeProvider>
      </section>
    )
  }
}

export default Positions
