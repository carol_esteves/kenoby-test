export const contains = (text, exp) => (
  text.match(new RegExp(exp, 'i'))
)

export default {
  contains
}
