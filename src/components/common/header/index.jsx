import React from 'react'
import PropTypes from 'prop-types'
import {
  AppBar,
  Toolbar,
  Typography,
  withStyles
} from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'
import BookmarkSwitch from '../switch'
import Search from '../search'

const Header = ({ classes, showBookmarked, toggleBookmark, search }) => {
  return (
    <AppBar position='static'>
      <div>
        <Toolbar className={classes.header}>
          <Typography variant='h1' className={classes.logo} color='inherit'>
            <span>
              <img src='https://s3-sa-east-1.amazonaws.com/prod-tenant-logos/55b7e031299d4e33019c1c5a' alt='Kenoby' />
            </span>
          </Typography>
          <Search onSearch={search} />
          <BookmarkSwitch showBookmarked={showBookmarked} toggleBookmark={toggleBookmark} />
          <div className='user'><AccountCircle style={{ fontSize: 30 }} /></div>
        </Toolbar>
      </div>
    </AppBar>
  )
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  showBookmarked: PropTypes.bool.isRequired,
  toggleBookmark: PropTypes.func.isRequired
}

const styles = {
  header: {
    maxWidth: '1070px',
    margin: '0 auto',

    '& .switch': {
      '&.checked > span + span': {
        background: 'white'
      },

      '& > span + span': {
        background: '#7316C4',
        opacity: 1
      },

      '&.checked > span > span > span': {
        '&:before': {
          color: '#9013FE'
        }
      },

      '& > span > span > span': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

        '&:before': {
          content: '"star"',
          fontFamily: 'Material Icons',
          color: '#F1F1F1',
          fontSize: '12px'
        }
      }
    }
  },

  logo: {
    flexGrow: 1,

    '& span': {
      display: 'flex',
      alignItems: 'center',

      '& img': {
        background: '#fff',
        padding: '5px',
        borderRadius: '2px',
        maxWidth: '100px'
      }
    }
  }
}

export default withStyles(styles)(Header)
