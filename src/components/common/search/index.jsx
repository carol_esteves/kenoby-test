import React from 'react'
import PropTypes from 'prop-types'
import {
  InputBase,
  withStyles
} from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'

const Search = ({ classes, onSearch }) => {
  const handleChange = onSearch => e => {
    const { value } = e.target
    onSearch(value)
  }

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder='Encontre uma vaga...'
        onChange={handleChange(onSearch)}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
      />
    </div>
  )
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  onSearch: PropTypes.func.isRequired
}

const styles = ({
  search: {
    position: 'relative',
    borderRadius: '3px',
    backgroundColor: '#AF58FC',
    marginLeft: '125px',
    width: '100%'
  },

  searchIcon: {
    width: '50px',
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  inputRoot: {
    color: 'inherit',
    width: '100%'
  },

  inputInput: {
    paddingTop: '10px',
    paddingRight: '10px',
    paddingBottom: '10px',
    paddingLeft: '50px',
    width: '100%',
    fontSize: '14px',
    fontWeight: 300,
    '&::placeholder': {
      opacity: 1
    }
  }
})

export default withStyles(styles)(Search)
