import React from 'react'
import PropTypes from 'prop-types'
import {
  Switch,
  withStyles
} from '@material-ui/core/'

const BookmarkSwitch = ({ showBookmarked, toggleBookmark }) => {
  const checked = showBookmarked ? ' checked' : ''
  return (
    <StyledSwitch
      className={`switch${checked}`}
      checked={showBookmarked}
      onChange={toggleBookmark}
      disableRipple
    />
  )
}

BookmarkSwitch.propTypes = {
  showBookmarked: PropTypes.bool.isRequired,
  toggleBookmark: PropTypes.func.isRequired
}

const StyledSwitch = withStyles({
  root: {
    margin: '0 125px 0 20px'
  },

  icon: {
    color: '#AC50FD'
  },

  iconChecked: {
    color: 'white'
  }
})(Switch)

export default BookmarkSwitch
