import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  withStyles
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import StarActiveIcon from '@material-ui/icons/Star'
import StarInactiveIcon from '@material-ui/icons/StarBorder'

const Position = ({ id, name, description, links, isBookmarked, handleBookmark, classes }) => {
  const handleClick = (handleBookmark, id) => e => {
    e.stopPropagation()
    handleBookmark(id)
  }

  const renderButtons = () => (
    links.map((l, i) => (
      <Button
        key={`${l}_${i}`}
        color='primary'
        variant='contained'
        href={l}
        target='_blank'>
        Candidate-se
      </Button>
    ))
  )

  return (
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon style={{ fill: '#ABABAB' }} />} className={classes.title}>
        <Typography>
          {name}
          <span
            onClick={handleClick(handleBookmark, id)}>
            {isBookmarked
              ? <StarActiveIcon style={{ fontSize: 18, fill: '#F2B449' }} />
              : <StarInactiveIcon style={{ fontSize: 18, fill: '#ABABAB' }} />}
          </span>
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div>
          <Typography dangerouslySetInnerHTML={{__html: description}} />
          {renderButtons()}
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

Position.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  links: PropTypes.array.isRequired,
  isBookmarked: PropTypes.bool.isRequired,
  handleBookmark: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
}

const styles = {
  title: {
    minHeight: '64px',

    '&[aria-expanded="true"]': {
      borderBottom: '1px solid #D3D3D3'
    },

    '& p': {
      display: 'flex',
      alignItems: 'center'
    },

    '& span': {
      fontSize: 0,
      paddingLeft: '5px'
    }
  }
}

export default withStyles(styles)(Position)
