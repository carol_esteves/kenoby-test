import React from 'react'
import PropTypes from 'prop-types'
import {
  CircularProgress,
  Typography,
  withStyles
} from '@material-ui/core'
import Position from './position'

const PositionsList = ({ loading, positions, bookmarked, handleBookmark, classes }) => {
  const renderPositions = positions => (
    positions.map(({ id, name, description, links }) => {
      return (
        <Position
          key={id}
          id={id}
          name={name}
          description={description}
          links={links}
          isBookmarked={bookmarked.includes(id)}
          handleBookmark={handleBookmark}
        />
      )
    })
  )

  if (loading) return <div className={classes.text}><CircularProgress /></div>
  if (!positions.length) return <div className={classes.text}><Typography>Não há vagas a serem exibidas</Typography></div>
  return (
    <div className={classes.list}>{renderPositions(positions)}</div>
  )
}

PositionsList.propTypes = {
  loading: PropTypes.bool.isRequired,
  positions: PropTypes.array.isRequired
}

const styles = {
  list: {
    maxWidth: '940px',
    margin: '30px auto',

    '& div[class*="expanded"]': {
      borderRadius: '2px'
    }
  },

  text: {
    textAlign: 'center',
    margin: '30px'
  }
}

export default withStyles(styles)(PositionsList)
