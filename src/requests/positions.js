import axios from 'axios'

const REQUEST_URL = 'https://back.kenoby.com/positions'
const requestHeader = {
  Accept: 'application/json',
  Authorization: 'Basic',
  'x-tenant': '55b7e031299d4e33019c1c5a',
  'x-version': '3.0.0'
}

export const getPositionsRequest = () => {
  return axios({
    method: 'GET',
    url: REQUEST_URL,
    headers: requestHeader
  })
}

export default {
  getPositionsRequest
}
